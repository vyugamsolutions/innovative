import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:innovative/griddashboard.dart';

import 'package:google_fonts/google_fonts.dart';

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: 'avenir'),
      home: dashPage(),
    );
  }
}

class dashPage extends StatefulWidget {
  @override
  _dashPageState createState() => _dashPageState();
}

class _dashPageState extends State<dashPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[800],
        elevation: 0.0,
        title: Text(
          "Dashboard",
          style: GoogleFonts.openSans(
              textStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 22,
                  fontWeight: FontWeight.bold)),
        ),
        actions: [
          // action button
          IconButton(
            alignment: Alignment.topCenter,
            icon: Icon(
              Icons.logout,
              color: Colors.white,
              size: 30,
            ),
            onPressed: () {},
          ),
        ],
      ),
      body: Column(
        children: <Widget>[SizedBox(height: 20), GridDashboard()],
      ),
    );
  }
}
