import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:intl/intl.dart';

class ProductionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: 'avenir'),
      home: productionscreen(),
    );
  }
}

class productionscreen extends StatefulWidget {
  @override
  _productionscreenState createState() => _productionscreenState();
}

class _productionscreenState extends State<productionscreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: const Text('Service Call Report'),
        centerTitle: true,
        backgroundColor: Colors.blue[800],
        elevation: 0.0,
      ),
      body: const SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 8.0),
          child: RegisterForm(),
        ),
      ),
    );
  }
}

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key key}) : super(key: key);

  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _agreedToTOS = true;
  bool _paymentReceived = true;
  List<String> _locations = [
    'Select Status',
    'Stand by',
    'Completed',
    'Pending',
    'To be Observed'
  ]; // Option 2
  String _selectedLocation = "Select Status"; // Option 2

  TextEditingController _controller = TextEditingController();
  TextEditingController _textWOMController = new TextEditingController();
  TextEditingController _textOPRController = new TextEditingController();
  TextEditingController _textFromController = new TextEditingController();
  TextEditingController _textToController = new TextEditingController();
  TextEditingController _textTimeController = new TextEditingController();
  String inputString = "";
  int camera = -1;
  String qrCodeResultWON;
  String qrCodeResultOPR;
  String _tovalue = '';
  String _fromvalue = '';
  TimeOfDay time;

  Future _selectFromDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(2020),
        lastDate: new DateTime(2099));
    if (picked != null)
      setState(() {
        _textFromController.text = DateFormat('dd-MM-yyyy').format(picked);
      });
  }

  Future _selectToDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(2020),
        lastDate: new DateTime(2099));
    if (picked != null)
      setState(() =>
          _textToController.text = DateFormat('dd-MM-yyyy').format(picked));
  }

  @override
  void initState() {
    super.initState();
    time = TimeOfDay.now();
  }

  Future _pickTime() async {
    TimeOfDay t = await showTimePicker(context: context, initialTime: time);
    if (t != null)
      setState(() {
        time = t;
        _textTimeController.text =
            time.hour.toString() + ":" + time.minute.toString();
      });
  }

  @override
  Widget build(BuildContext context) {
    return _getFormWidget();
  }

  bool _submittable() {
    return _agreedToTOS;
  }

  void _submit() {
    _formKey.currentState.validate();
    print('Form submitted');
  }

  void _setAgreedToTOS(bool newValue) {
    setState(() {
      _agreedToTOS = newValue;
    });
  }

  void _setPaymentRecived(bool newValue) {
    setState(() {
      _paymentReceived = newValue;
    });
  }

  Widget _getFormWidget() {
    return Column(children: [
      TextField(
          controller: _textWOMController,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.only(
                top: 20, left: 10), // add padding to adjust text
            isDense: true,
            hintText: "Work Order Number",

            suffixIcon: GestureDetector(
              child: Padding(
                padding: EdgeInsets.only(
                    right: 10, left: 10), // add padding to adjust icon
                child: Icon(Icons.qr_code, size: 40),
              ),
              onTap: () async {
                ScanResult codeSanner = await BarcodeScanner.scan(
                  options: ScanOptions(
                    useCamera: camera,
                  ),
                ); //barcode scnner
                setState(() {
                  qrCodeResultWON = codeSanner.rawContent;
                  _textWOMController.text = qrCodeResultWON;
                });
              },
            ),
          ),
          style: GoogleFonts.openSans(
              textStyle: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w600))),
      SizedBox(height: 14),
      TextField(
          controller: _textFromController,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.only(
                top: 20, left: 10), // add padding to adjust text
            isDense: true,
            hintText: "Date",

            suffixIcon: GestureDetector(
                child: Padding(
                  padding: EdgeInsets.only(
                      right: 10, left: 10), // add padding to adjust icon
                  child: Icon(Icons.date_range, size: 40),
                ),
                onTap: () => {_selectFromDate()}),
          ),
          style: GoogleFonts.openSans(
              textStyle: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w600))),
      SizedBox(height: 14),
      TextField(
          keyboardType: TextInputType.multiline,
          minLines: 1, //Normal textInputField will be displayed
          maxLines: 3,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.only(
                top: 10, left: 10), // add padding to adjust text
            isDense: true,
            hintText: "Customer",

            prefixIcon: GestureDetector(
              child: Padding(
                padding: EdgeInsets.only(
                    right: 10, left: 5), // add padding to adjust icon
                child: Icon(Icons.supervised_user_circle, size: 30),
              ),
              onTap: () => print("Hello"),
            ),
          ),
          style: GoogleFonts.openSans(
              textStyle: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w600))),
      SizedBox(height: 14),
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: Row(
          children: <Widget>[
            Text("Call Type",
                style: GoogleFonts.openSans(
                    textStyle: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w600))),
            Checkbox(
              value: _agreedToTOS,
              onChanged: _setAgreedToTOS,
            ),
            GestureDetector(
              onTap: () => _setAgreedToTOS(!_agreedToTOS),
              child: Text('Chargeable',
                  style: GoogleFonts.openSans(
                      textStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w600))),
            ),
          ],
        ),
      ),
      SizedBox(height: 14),
      TextField(
          keyboardType: TextInputType.multiline,
          minLines: 1, //Normal textInputField will be displayed
          maxLines: 3,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.only(
                top: 10, left: 10), // add padding to adjust text
            isDense: true,
            hintText: "Compliant",

            prefixIcon: GestureDetector(
              child: Padding(
                padding: EdgeInsets.only(
                    right: 10, left: 5), // add padding to adjust icon
                child: Icon(Icons.warning, size: 30),
              ),
              onTap: () => print("Hello"),
            ),
          ),
          style: GoogleFonts.openSans(
              textStyle: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w600))),
      SizedBox(height: 14),
      _getBodyWidget(),
      TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.only(
                top: 10, left: 10), // add padding to adjust text
            isDense: true,
            hintText: "Action Taken",

            prefixIcon: GestureDetector(
              child: Padding(
                padding: EdgeInsets.only(
                    right: 10, left: 5), // add padding to adjust icon
                child: Icon(Icons.star_border, size: 30),
              ),
              onTap: () => print("Hello"),
            ),
          ),
          style: GoogleFonts.openSans(
              textStyle: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w600))),
      SizedBox(height: 14),
      TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.only(
                top: 10, left: 10), // add padding to adjust text
            isDense: true,
            hintText: "Spares Replaced",

            prefixIcon: GestureDetector(
              child: Padding(
                padding: EdgeInsets.only(
                    right: 10, left: 5), // add padding to adjust icon
                child: Icon(Icons.replay_circle_filled, size: 30),
              ),
              onTap: () => print("Hello"),
            ),
          ),
          style: GoogleFonts.openSans(
              textStyle: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w600))),
      SizedBox(height: 14),
      TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.only(
                top: 20, left: 10), // add padding to adjust text
            isDense: true,
            hintText: "Customer Remarks",

            prefixIcon: GestureDetector(
              child: Padding(
                padding: EdgeInsets.only(
                    right: 10, left: 5), // add padding to adjust icon
                child: Icon(Icons.feedback, size: 30),
              ),
              onTap: () => print("Hello"),
            ),
          ),
          style: GoogleFonts.openSans(
              textStyle: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w600))),
      SizedBox(height: 14),
      TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.only(
                top: 20, left: 10), // add padding to adjust text
            isDense: true,
            hintText: "Spares Cost",

            prefixIcon: GestureDetector(
              child: Padding(
                padding: EdgeInsets.only(
                    right: 10, left: 5), // add padding to adjust icon
                child: Icon(Icons.attach_money, size: 30),
              ),
              onTap: () => print("Hello"),
            ),
          ),
          style: GoogleFonts.openSans(
              textStyle: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w600))),
      SizedBox(height: 14),
      TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.only(
                top: 20, left: 10), // add padding to adjust text
            isDense: true,
            hintText: "Services Cost",

            prefixIcon: GestureDetector(
              child: Padding(
                padding: EdgeInsets.only(
                    right: 10, left: 5), // add padding to adjust icon
                child: Icon(Icons.attach_money, size: 30),
              ),
              onTap: () => print("Hello"),
            ),
          ),
          style: GoogleFonts.openSans(
              textStyle: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w600))),
      SizedBox(height: 14),
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: Row(
          children: <Widget>[
            Text("Payemnt Received",
                style: GoogleFonts.openSans(
                    textStyle: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w600))),
            Checkbox(
              value: _paymentReceived,
              onChanged: _setPaymentRecived,
            ),
            GestureDetector(
              onTap: () => _setPaymentRecived(!_paymentReceived),
              child: Text('Yes',
                  style: GoogleFonts.openSans(
                      textStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w600))),
            ),
          ],
        ),
      ),
      SizedBox(height: 14),
      InputDecorator(
        decoration: InputDecoration(
          labelText: '',
          labelStyle: Theme.of(context)
              .primaryTextTheme
              .caption
              .copyWith(color: Colors.black),
          border: const OutlineInputBorder(),
        ),
        child: DropdownButtonHideUnderline(
          child: DropdownButton(
            isExpanded: true,
            isDense: true, // Reduces the dropdowns height by +/- 50%
            icon: Icon(Icons.keyboard_arrow_down),
            value: _selectedLocation,
            items: _locations.map((item) {
              return DropdownMenuItem(
                value: item,
                child: Text(item),
              );
            }).toList(),
            onChanged: (selectedItem) => setState(
              () => _selectedLocation = selectedItem,
            ),
          ),
        ),
      ),
      Row(
        children: [
          Container(
              padding: const EdgeInsets.only(left: 50.0, top: 50.0),
              child: RaisedButton(
                onPressed: () {},
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(80.0)),
                padding: const EdgeInsets.all(0.0),
                child: Ink(
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [Colors.greenAccent, Colors.lightGreenAccent]),
                    borderRadius: BorderRadius.all(Radius.circular(80.0)),
                  ),
                  child: Container(
                    constraints: const BoxConstraints(
                        minWidth: 120.0,
                        minHeight: 50.0), // min sizes for Material buttons
                    alignment: Alignment.center,
                    child: const Text(
                      'Submit',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
              )),
          Container(
              padding: const EdgeInsets.only(left: 50.0, top: 50.0),
              child: RaisedButton(
                onPressed: () {},
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(80.0)),
                padding: const EdgeInsets.all(0.0),
                child: Ink(
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [Colors.redAccent, Colors.red]),
                    borderRadius: BorderRadius.all(Radius.circular(80.0)),
                  ),
                  child: Container(
                    constraints: const BoxConstraints(
                        minWidth: 120.0,
                        minHeight: 50.0), // min sizes for Material buttons
                    alignment: Alignment.center,
                    child: const Text(
                      'Cencel',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
              )),
        ],
      ),
    ]);
  }

  Widget _getBodyWidget() {
    return Padding(
      padding: const EdgeInsets.all(6.0),
      child: Container(
        child: HorizontalDataTable(
          leftHandSideColumnWidth: 150,
          rightHandSideColumnWidth: 400,
          isFixedHeader: true,
          headerWidgets: _getTitleWidget(),
          leftSideItemBuilder: _generateFirstColumnRow,
          rightSideItemBuilder: _generateRightHandSideColumnRow,
          itemCount: 2,
          rowSeparatorWidget: const Divider(
            color: Colors.black54,
            height: 1.0,
            thickness: 0.0,
          ),
        ),
        height: 160,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('Actions', 150),
      _getTitleItemWidget('Date', 200),
      _getTitleItemWidget('Time', 200),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      child: Text(label, style: TextStyle(fontWeight: FontWeight.bold)),
      width: width,
      height: 56,
      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text((index == 0)
          ? 'Call Attended'
          : (index == 1)
              ? 'Call Completed'
              : ''),
      width: 150,
      height: 52,
      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget _generateFirstColumnRowDUPLICATE(BuildContext context, int index) {
    return Container(
      child: InputDecorator(
        decoration: InputDecoration(
          labelText: '',
          labelStyle: Theme.of(context)
              .primaryTextTheme
              .caption
              .copyWith(color: Colors.black),
          border: const OutlineInputBorder(),
        ),
        child: DropdownButtonHideUnderline(
          child: DropdownButton(
            isExpanded: true,
            isDense: true, // Reduces the dropdowns height by +/- 50%
            icon: Icon(Icons.keyboard_arrow_down),
            value: _selectedLocation,
            items: _locations.map((item) {
              return DropdownMenuItem(
                value: item,
                child: Text(item),
              );
            }).toList(),
            onChanged: (selectedItem) => setState(
              () => _selectedLocation = selectedItem,
            ),
          ),
        ),
      ),
      width: 200,
      height: 52,
      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: TextField(
              controller: _textToController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                contentPadding: EdgeInsets.only(
                    top: 20, left: 10), // add padding to adjust text
                isDense: true,
                hintText: "Date",

                suffixIcon: GestureDetector(
                    child: Padding(
                      padding: EdgeInsets.only(
                          right: 10, left: 10), // add padding to adjust icon
                      child: Icon(Icons.date_range, size: 40),
                    ),
                    onTap: () => {_selectToDate()}),
              ),
              style: GoogleFonts.openSans(
                  textStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.w600))),
          width: 200,
          height: 52,
          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: TextField(
              controller: _textTimeController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: EdgeInsets.only(
                      top: 20, left: 10), // add padding to adjust text
                  isDense: true,
                  hintText: "Time",
                  suffixIcon: GestureDetector(
                      child: Padding(
                        padding: EdgeInsets.only(
                            right: 10, left: 10), // add padding to adjust icon
                        child: Icon(Icons.date_range, size: 40),
                      ),
                      onTap: () => {_pickTime()})),
              style: GoogleFonts.openSans(
                  textStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.w600))),
          width: 200,
          height: 52,
          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
          alignment: Alignment.centerLeft,
        ),
      ],
    );
  }

  Widget _generateBuild(BuildContext context, int index) {
    return AlertDialog(
      title: Text("Server Ip Address"),
      content: TextFormField(
        controller: _controller,
      ),
      actions: <Widget>[
        FlatButton(
          child: Text("Submit"),
          onPressed: () {
            Navigator.pop(context, _controller.text);
          },
        )
      ],
    );
  }
}
