import 'package:flutter/material.dart';
import 'package:innovative/ProductionScreen.dart';
import 'package:google_fonts/google_fonts.dart';

class GridDashboard extends StatelessWidget {
  Items item1 = new Items(
    title: "Service Call Reports",
    event: "servicecallreport",
    img: "asset/checklist.png",
  );

  @override
  Widget build(BuildContext context) {
    List<Items> myList = [item1];
    var color = 0xfffffffe;
    return Flexible(
      child: GridView.count(
          childAspectRatio: 1.0,
          padding: EdgeInsets.only(left: 16, right: 16),
          crossAxisCount: 2,
          crossAxisSpacing: 18,
          mainAxisSpacing: 18,
          children: myList.map((data) {
            return InkWell(
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black26),
                    color: Color(color),
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      data.img,
                      width: 54,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      data.title,
                      style: GoogleFonts.openSans(
                          textStyle: TextStyle(
                              color: Colors.black,
                              fontSize: 17,
                              fontWeight: FontWeight.w600)),
                    ),
                  ],
                ),
              ),
              onTap: () {
                if (data.event == "servicecallreport") {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => productionscreen()));
                } else if (data.event == "inventorytransfer") {
                  print("clicksI");
                } else if (data.event == "reports") {
                  print("clicksR");
                }
              },
            );
          }).toList()),
    );
  }
}

class Items {
  String title;
  String subtitle;
  String event;
  String img;
  Items({this.title, this.subtitle, this.event, this.img});
}
